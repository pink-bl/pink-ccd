#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## test
epicsEnvSet("IOCBL", "PINK")
epicsEnvSet("IOCDEV", "GEYES")
epicsEnvSet("DEVIP", "172.17.10.57")


# Prefix for all records
epicsEnvSet("PREFIX", "$(IOCBL):$(IOCDEV):")
# The port name for the detector
epicsEnvSet("PORT",   "GE")
# The queue size for all plugins
epicsEnvSet("QSIZE",  "20")
# The maximim image width; used for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",  "1024")
# The maximim image height; used for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",  "255")
# The maximum number of time seried points in the NDPluginStats plugin
epicsEnvSet("NCHANS", "2048")
# The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS", "10")
# The search path for database files
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db")
epicsEnvSet("NELEMENTS", "270000")

asynSetMinTimerPeriod(0.001)
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "3000000")

# Create a GreatEyesCCD driver (IMPORTANT: set the correct PeltierType parameter
#/** IOC shell configuration command for GreatEyes driver
#/**
#  * \param[in] portName The name of the asyn port driver to be created.
#  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
#  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
#  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
#  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
#  * \param[in] peltierType (configuration Number of cooling Hardware, supplied with your camera. If unknown contact greateyes GmbH )
#  * \param[in] InterfaceType (specifies if the camera is directly connected via USB, or controlled via a greateyesCameraServer Software)
#  *                           0 - USB
#  *                           3 - greateyesCameraServer (TCP/IP)
#  * \param[in] IPAddress if the camera is controlled via greateyesCameraServer, you have to specify the IP of the server.
#  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
#  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
#  */
#GreatEyesCCD::GreatEyesCCD(const char *portName, int maxBuffers, size_t maxMemory,
#                           int peltierType, int interfaceType, const char* IPAddress, int priority, int stackSize)

# Configuration for greateyesCameraServer
greatEyesCCDConfig("$(PORT)", -1, -1, 42223, 3, "$(DEVIP)", 0, 0)

## test
## drvAsynIPPortConfigure("$(PORT)", "$(DEVIP):12345", 0, 0, 0)

## Load record instances
dbLoadRecords("$(ADGEYES)/db/greatEyesCCD.template","P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=2,RATE_SMOOTH=0.9")

## Images
#NDStdArraysConfigure("Image1", 5, 0, "$(PORT)", 0, 0)
#NDStdArraysConfigure("Image2", 5, 0, "$(PORT)", 0, 0)
#NDStdArraysConfigure("Image3", 5, 0, "$(PORT)", 0, 0)
#NDStdArraysConfigure("Image4", 5, 0, "$(PORT)", 0, 0)
#NDStdArraysConfigure("Image5", 5, 0, "$(PORT)", 0, 0)
#NDStdArraysConfigure("Image6", 5, 0, "$(PORT)", 0, 0)

#dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image2:,PORT=Image2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image3:,PORT=Image3,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image4:,PORT=Image4,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image5:,PORT=Image5,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")
#dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image6:,PORT=Image6,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(NELEMENTS)")

# Load some extra records for PINK Beamline
#dbLoadRecords("${TOP}/iocBoot/${IOC}/geyessup.db", "P=$(PREFIX), BL=$(IOCBL), DEV=$(IOCDEV)")

cd "${TOP}/iocBoot/${IOC}"

## load areaDetector plugins
#< plugins.cmd

## AUTOSAVE CONFIG
set_requestfile_path("./")
set_requestfile_path("$(ADCORE)/ADApp/Db")
set_requestfile_path("$(CALC)/calcApp/Db")

set_savefile_path("/EPICS/autosave")
#set_pass0_restoreFile("auto_settings.sav")
#set_pass1_restoreFile("auto_settings.sav")

iocInit

# save things every thirty seconds
#create_monitor_set("auto_settings.req", 30,"P=$(PREFIX)")

## Start any sequence programs
#seq sncxxx,"user=epics"
